package com.example.laondry.repository;

import com.example.laondry.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, Integer>{
    Admin findByUsername(String username);
}
