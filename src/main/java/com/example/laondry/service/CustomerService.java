package com.example.laondry.service;

import com.example.laondry.entity.Customer;
import com.example.laondry.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;


    public Customer saveCust(Customer customer) {
        return customerRepository.save(customer);
    }

    public List<Customer> getCust() {
        return customerRepository.findAll();
    }

    public String deleteCust(int id) {
        customerRepository.deleteById(id);
        return "Customer Remove";
    }

    public Customer updateCust(Customer customer) {
        Customer existingCustomer = (Customer) customerRepository.findById(customer.getIdCustomer()).orElse(null);
        existingCustomer.setNama(customer.getNama());
        existingCustomer.setTanggal(customer.getTanggal());
        existingCustomer.setBerat(customer.getBerat());
        existingCustomer.setKodePemesanan(customer.getKodePemesanan());
        return (Customer) customerRepository.save(existingCustomer);
    }

    public Optional<Customer> findById(int id) {
        return customerRepository.findById(id);
    }
}
