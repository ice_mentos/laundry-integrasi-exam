package com.example.laondry.controller;

import com.example.laondry.entity.Admin;
import com.example.laondry.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AdminController {
    @Autowired
    private AdminService adminService;

    @GetMapping("/admin")
    public List<Admin> findAllAdmin(){
        return adminService.getAdmin();
    }

    @GetMapping("/admin/{username}")
    public Admin findbyUsername(@PathVariable String username){
        return  adminService.getUsername(username);
    }
}
