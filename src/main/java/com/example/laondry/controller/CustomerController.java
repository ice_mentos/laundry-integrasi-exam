package com.example.laondry.controller;

import com.example.laondry.entity.Customer;
import com.example.laondry.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CustomerController {
    @Autowired
    private CustomerService customerservice;

    //AddCust
    @PostMapping("/addCust")
    public Customer addCust(@RequestBody Customer customer){
        return customerservice.saveCust(customer);
    }

    //GetCust
    @GetMapping("/cust")
    public List<Customer> findAllCust(){
        return customerservice.getCust();
    }

    //DeleteCust
    @DeleteMapping("/delete/{id}")
    public String deleteCust(@PathVariable int id){
        return customerservice.deleteCust(id);
    }

    //UpdateCust
    @PutMapping("/update/{id}")
    public Customer updateCust(@RequestBody Customer customer, @PathVariable int id){
        customer.setIdCustomer(id);
        return customerservice.updateCust(customer);
    }

    @GetMapping("/cust/{id}")
    public Optional<Customer> findById(@PathVariable int id){
        return customerservice.findById(id);
    }

//    //Search
//    @GetMapping("/cust/{id}")
//    public Optional<Customer> findByid (@PathVariable int id){
//        return customerservice.findByKode(id);
//    }
}
